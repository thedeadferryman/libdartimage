//
// Created by kreme on 02.03.2021.
//

#ifndef DARTCAIRO_SVGIMAGE_H
#define DARTCAIRO_SVGIMAGE_H

#include <glib-object.h>
#include <librsvg/rsvg.h>

#include "../definitions.h"

class SVGImage {
public:
	RsvgHandle* handle;

private:
	CDEF_EXPORT_DartImage_ErrorStatus status = CDEF_EXPORT_DartImage_ErrorStatus::STATUS_OK;

public:
	explicit SVGImage(const char *path);

	int32_t getWidth();

	int32_t getHeight();

	CDEF_EXPORT_DartImage_ErrorStatus getStatus();

	~SVGImage();

	friend class CairoImage;
};


#endif //DARTCAIRO_SVGIMAGE_H
