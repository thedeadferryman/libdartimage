//
// Created by kreme on 02.03.2021.
//

#ifndef DARTCAIRO_CAIROIMAGE_H
#define DARTCAIRO_CAIROIMAGE_H

#include <cairomm/cairomm.h>
#include <pangomm.h>
#include <string>
#include <cstdint>

#include "../definitions.h"
#include "Color.h"
#include "SVGImage.h"
#include "FontUtils.h"

template<typename T>
using RefPtr = Cairo::RefPtr<T>;
using Format = Cairo::Format;
using CairoContext = Cairo::Context;
using ImageSurface = Cairo::ImageSurface;

class CairoImage {
private:
	RefPtr<CairoContext> context;
	CDEF_EXPORT_DartImage_ErrorStatus status = CDEF_EXPORT_DartImage_ErrorStatus::STATUS_OK;

	constexpr static const double OUTLINE_LINE_WIDTH_COEF = 0.07;
	constexpr static const int32_t OUTLINE_LETTER_SPACING_COEF = 16;
public:
	CairoImage(int32_t width, int32_t height, Format format = Format::FORMAT_RGB24);

	explicit CairoImage(const String &path);

	CairoImage(CairoImage &other);

	CairoImage(CairoImage &&other) noexcept;

private:
	explicit CairoImage(const RefPtr<ImageSurface> &input);

public:
	double getWidth();

	double getHeight();

	Format getPixFmt();

	CDEF_EXPORT_DartImage_ErrorStatus getStatus();

	void drawImage(CairoImage image, double x, double y);

	void drawImage(CairoImage image, double x, double y, double scale);

	void drawImage(CairoImage image, double x, double y, double w, double h);

	void drawSVG(SVGImage image, double x, double y, double scale);

	void drawSVG(SVGImage image, double x, double y, double w, double h);

	void fill(Color color);

	CairoImage resize(double scale);

	CairoImage resize(double w, double h);

	CairoImage clone();

	ErrorStatus saveAsPNG(const String &path);

	void drawText(const std::string &text,
	              const FontDescription &font, const Color &color,
	              int32_t x, int32_t y,
	              bool outline = false);

	CDEF_EXPORT_FontUtils_TextExtents getTextExtents(const std::string &text, const FontDescription &font);

private:
	RefPtr<ImageSurface> getSurface();

	void drawSVG(SVGImage image, RsvgRectangle rect);
};

#endif //DARTCAIRO_CAIROIMAGE_H

