//
// Created by kreme on 04.03.2021.
//

#ifndef DARTIMAGE_FONTUTILS_H
#define DARTIMAGE_FONTUTILS_H

#include <pangomm.h>
#include "../definitions.h"

using FontDescription = Pango::FontDescription;

using TextLayout = Pango::Layout;

FontDescription buildFont(const String &fontFace,
                          CDEF_EXPORT_FontUtils_FontSlant slant, CDEF_EXPORT_FontUtils_FontWeight weight,
                          double fontSize);

int32_t getFontSize(const FontDescription &font);

#endif //DARTIMAGE_FONTUTILS_H
