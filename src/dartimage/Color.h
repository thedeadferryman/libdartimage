//
// Created by kreme on 02.03.2021.
//

#ifndef DARTCAIRO_RGB_H
#define DARTCAIRO_RGB_H

#include <cstdint>


class Color {
public:
	double r;
	double g;
	double b;
	double a;
public:
	Color(int32_t r, int32_t g, int32_t b, double a = 1);
	explicit Color(int32_t gray, double a = 1);

	[[nodiscard]] Color getOutlineColor() const;
	[[nodiscard]] Color getGrayscale() const;
	[[nodiscard]] Color getOpposite() const;


private:
	Color(double r, double g, double b, double a);
	explicit Color(double gray, double a);
};

#endif //DARTCAIRO_RGB_H
