//
// Created by kreme on 02.03.2021.
//

#include "CairoImage.h"

#include <algorithm>
#include <typeindex>

using namespace std;

using Extend = Cairo::Extend;
using SurfacePattern = Cairo::SurfacePattern;
using LineCap = Cairo::LineCap;
using LogicError = Cairo::logic_error;

CairoImage::CairoImage(int32_t width, int32_t height, Format format) {
	auto surface = ImageSurface::create(format, width, height);
	context = CairoContext::create(surface);
}

CairoImage::CairoImage(const String &path) {
	try {
		auto surface = ImageSurface::create_from_png(path);
		context = CairoContext::create(surface);
	} catch (LogicError &err) {
		switch (err.get_status_code()) {
		case CAIRO_STATUS_FILE_NOT_FOUND:
			status = ErrorStatus::STATUS_FILE_NOT_FOUND;
			break;
		default:
			status = ErrorStatus::STATUS_CAIRO_UNEXPECTED_ERROR;
		}
	}
}

CairoImage::CairoImage(CairoImage &other) : CairoImage(other.getSurface()) {}

CairoImage::CairoImage(const RefPtr<ImageSurface> &input) {
	auto format = input->get_format();
	auto width = input->get_width();
	auto height = input->get_height();

	auto surface = ImageSurface::create(format, width, height);
	context = CairoContext::create(surface);

	context->save();

	context->set_source(input, 0, 0);

	context->rectangle(0, 0, width, height);
	context->clip();
	context->paint();
	context->reset_clip();

	context->restore();
}

CairoImage::CairoImage(CairoImage &&other) noexcept = default;

double CairoImage::getWidth() {
	return getSurface()->get_width();
}

double CairoImage::getHeight() {
	return getSurface()->get_height();
}

void CairoImage::drawImage(CairoImage image, double x, double y) {
	drawImage(image, x, y, 1);
}

void CairoImage::drawImage(CairoImage image, double x, double y, double scale) {
	context->save();

	context->set_source(image.getSurface(), 0, 0);

	context->translate(x, y);
	context->scale(scale, scale);

	auto pattern = SurfacePattern::create(image.getSurface());
	pattern->set_extend(Extend::EXTEND_NONE);

	context->set_source(pattern);

	context->rectangle(0, 0,
	                   image.getWidth(),
	                   image.getHeight()
	);

	context->fill();

	context->restore();
}

void CairoImage::drawImage(CairoImage image, double x, double y, double w, double h) {
	auto scale = std::min(
			w / image.getWidth(),
			h / image.getHeight()
	);

	drawImage(image, x, y, scale);
}

CairoImage CairoImage::clone() {
	return CairoImage(getSurface());
}

RefPtr<ImageSurface> CairoImage::getSurface() {
	return RefPtr<ImageSurface>::cast_static(context->get_target());
}

ErrorStatus CairoImage::saveAsPNG(const String &path) {
	try {
		getSurface()->write_to_png(path);

		return ErrorStatus::STATUS_OK;
	} catch (const std::exception &e) {
		if (std::type_index(typeid(e)) == std::type_index(typeid(ios_base::failure))) {
			return ErrorStatus::STATUS_FILE_NOT_WRITABLE;
		}

		return ErrorStatus::STATUS_CAIRO_UNEXPECTED_ERROR;
	}
}

CairoImage CairoImage::resize(double scale) {
	auto newWidth = getWidth() * scale;
	auto newHeight = getHeight() * scale;

	auto image = CairoImage(newWidth, newHeight, getPixFmt());

	image.drawImage(*this, 0, 0, scale);

	return image;
}

CairoImage CairoImage::resize(double w, double h) {
	auto scale = std::min(
			w / getWidth(),
			h / getHeight()
	);

	return resize(scale);
}

Format CairoImage::getPixFmt() {
	return getSurface()->get_format();
}

void CairoImage::fill(Color color) {
	context->save();

	context->set_source_rgba(color.r, color.g, color.b, color.a);
	context->rectangle(0, 0, getWidth(), getHeight());

	context->paint();

	context->restore();
}

void CairoImage::drawSVG(SVGImage image, double x, double y, double scale) {
	const RsvgRectangle rect {
			x, y,
			image.getWidth() * scale,
			image.getHeight() * scale,
	};

	drawSVG(image, rect);
}

void CairoImage::drawSVG(SVGImage image, double x, double y, double w, double h) {
	const RsvgRectangle rect {x, y, w, h};

	drawSVG(image, rect);
}

void CairoImage::drawSVG(SVGImage image, RsvgRectangle rect) {
	rsvg_handle_render_document(image.handle, context->cobj(), &rect, nullptr);
}

void CairoImage::drawText(const std::string &text,
                          const FontDescription &font, const Color &color,
                          int32_t x, int32_t y,
                          bool outline) {
	auto layout = TextLayout::create(context);

	if (outline) {
		auto list = Pango::AttrList();
		auto attr = Pango::AttrInt::create_attr_letter_spacing(font.get_size() / OUTLINE_LETTER_SPACING_COEF);

		list.insert(attr);

		layout->set_attributes(list);
	}

	layout->set_font_description(font);

	layout->set_text(text);

	context->save();

	context->begin_new_path();
	context->move_to(x, y);

	layout->add_to_cairo_context(context);

	if (outline) {
		auto outlineColor = color.getOutlineColor();

		context->set_source_rgba(outlineColor.r, outlineColor.g,
		                         outlineColor.b, outlineColor.a);

		context->set_line_cap(LineCap::LINE_CAP_ROUND);

		context->set_line_width(getFontSize(font) * OUTLINE_LINE_WIDTH_COEF);

		context->stroke_preserve();
	}

	context->set_source_rgba(color.r, color.g,
	                         color.b, color.a);

	context->close_path();

	context->fill();

	context->restore();
}

CDEF_EXPORT_FontUtils_TextExtents CairoImage::getTextExtents(const std::string &text, const FontDescription &font) {
	auto layout = TextLayout::create(context);

	layout->set_font_description(font);

	layout->set_text(text);

	CDEF_EXPORT_FontUtils_TextExtents xt;

	layout->get_pixel_size(xt.width, xt.height);

	return xt;
}

CDEF_EXPORT_DartImage_ErrorStatus CairoImage::getStatus() {
	return status;
}

