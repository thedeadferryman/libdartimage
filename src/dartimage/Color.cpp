//
// Created by kreme on 04.03.2021.
//

#include "Color.h"

Color::Color(int32_t r, int32_t g, int32_t b, double a) : Color(r / 256.0,
                                                          g / 256.0,
                                                          b / 256.0,
                                                                a) {}

Color::Color(double r, double g, double b, double a) : r(r), g(g), b(b), a(a) {}

Color::Color(int32_t gray, double a) : Color(gray / 256.0, a) {}

Color::Color(double gray, double a) : r(gray), g(gray), b(gray), a(a) {}

Color Color::getOutlineColor() const {
	return getGrayscale().getOpposite();
}

Color Color::getOpposite() const {
	return Color(1.0 - r,
	           1.0 - g,
	           1.0 - b,
	             a);
}

Color Color::getGrayscale() const {
	return Color((r + g + b) / 3, a);
}

