//
// Created by kreme on 04.03.2021.
//

#include "FontUtils.h"

using PangoFontSlant = Pango::Style;
using PangoFontWeight = Pango::Weight;

PangoFontSlant slantToPango(CDEF_EXPORT_FontUtils_FontSlant slant) {
	switch (slant) {
	case CDEF_EXPORT_FontUtils_FontSlant::SLANT_OBLIQUE:
		return PangoFontSlant::STYLE_OBLIQUE;
	case CDEF_EXPORT_FontUtils_FontSlant::SLANT_ITALIC:
		return PangoFontSlant::STYLE_ITALIC;
	case CDEF_EXPORT_FontUtils_FontSlant::SLANT_NORMAL:
	default:
		return PangoFontSlant::STYLE_NORMAL;
	}
}

PangoFontWeight weightToPango(CDEF_EXPORT_FontUtils_FontWeight weight) {
	switch (weight) {
	case CDEF_EXPORT_FontUtils_FontWeight::WEIGHT_BOLD:
		return PangoFontWeight::WEIGHT_BOLD;
	case CDEF_EXPORT_FontUtils_FontWeight::WEIGHT_NORMAL:
	default:
		return PangoFontWeight::WEIGHT_NORMAL;
	}
}

FontDescription buildFont(const String &fontFace,
                          CDEF_EXPORT_FontUtils_FontSlant slant, CDEF_EXPORT_FontUtils_FontWeight weight,
                          double fontSize) {
	FontDescription fd;

	fd.set_family(fontFace);

	fd.set_style(slantToPango(slant));
	fd.set_weight(weightToPango(weight));

	fd.set_absolute_size(fontSize * PANGO_SCALE);

	return fd;
}

int32_t getFontSize(const FontDescription &font) {
	return font.get_size() / 1024;
}
