//
// Created by kreme on 02.03.2021.
//

#include "SVGImage.h"

SVGImage::SVGImage(const char *path) {
	GError *err = nullptr;

	handle = rsvg_handle_new_from_file(path, &err);

	if (err != nullptr) {
		if (err->domain == RSVG_ERROR) {
			status = CDEF_EXPORT_DartImage_ErrorStatus::STATUS_RSVG_UNEXPECTED_ERROR;
		} else {
			status = CDEF_EXPORT_DartImage_ErrorStatus::STATUS_GLIB_UNEXPECTED_ERROR;
		}

		g_error_free(err);
	} else {
		rsvg_handle_set_dpi(handle, 96);
	}
}

SVGImage::~SVGImage() {
	g_object_unref(handle);
};

int32_t SVGImage::getWidth() {
	RsvgDimensionData dims;

	rsvg_handle_get_dimensions(handle, &dims);

	return dims.width;
}

int32_t SVGImage::getHeight() {
	RsvgDimensionData dims;

	rsvg_handle_get_dimensions(handle, &dims);

	return dims.height;
}

CDEF_EXPORT_DartImage_ErrorStatus SVGImage::getStatus() {
	return status;
}
