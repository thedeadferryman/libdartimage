#ifndef DARTCAIRO_DARTCAIRO_H
#define DARTCAIRO_DARTCAIRO_H

#include "cdef/CCairoImage.h"
#include "cdef/CSVGImage.h"
#include "cdef/CFontUtils.h"

CDEF_START

void CDEF_EXPORT_DartImage_init();

CDEF_END

#endif //DARTCAIRO_DARTCAIRO_H
