//
// Created by kreme on 02.03.2021.
//

#ifndef DARTCAIRO_DEFINITIONS_H
#define DARTCAIRO_DEFINITIONS_H

#include <fontconfig/fontconfig.h>
#include <cairo/cairo.h>
#include <stdint.h> // NOLINT(modernize-deprecated-headers)
#include <stdbool.h> // NOLINT(modernize-deprecated-headers)

#define String std::string

#ifdef __cplusplus
#define CDEF_START extern "C" {
#define CDEF_END };
#else
#define CDEF_START
#define CDEF_END
#endif

#ifndef __cplusplus
#ifndef bool
#define bool _Bool
#define true 1
#define false 0
#endif
#endif

CDEF_START

typedef enum {
	SLANT_NORMAL = FC_SLANT_ROMAN,
	SLANT_ITALIC = FC_SLANT_ITALIC,
	SLANT_OBLIQUE = FC_SLANT_OBLIQUE
} CDEF_EXPORT_FontUtils_FontSlant;

typedef enum {
	WEIGHT_NORMAL = FC_WEIGHT_NORMAL,
	WEIGHT_BOLD = FC_WEIGHT_BOLD
} CDEF_EXPORT_FontUtils_FontWeight;

typedef struct TextExtents {
	int32_t width;
	int32_t height;
} CDEF_EXPORT_FontUtils_TextExtents;

typedef enum PixFormat {
	FORMAT_ARGB32 = CAIRO_FORMAT_ARGB32,
	FORMAT_RGB24 = CAIRO_FORMAT_RGB24,
} CDEF_EXPORT_CairoImage_PixFormat;

typedef enum ErrorStatus {
	STATUS_OK,
	STATUS_FILE_NOT_FOUND,
	STATUS_FILE_NOT_READABLE,
	STATUS_FILE_NOT_WRITABLE,
	STATUS_CAIRO_UNEXPECTED_ERROR,
	STATUS_RSVG_UNEXPECTED_ERROR,
	STATUS_GLIB_UNEXPECTED_ERROR,
} CDEF_EXPORT_DartImage_ErrorStatus;

CDEF_END

#endif //DARTCAIRO_DEFINITIONS_H
