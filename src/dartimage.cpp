//
// Created by Karl Meinkopf on 09.03.2021.
//

#include <glibmm/init.h>
#include <pangomm/wrap_init.h>

#include "dartimage.h"

void CDEF_EXPORT_DartImage_init() {
	Glib::init();
	Pango::wrap_init();
}

