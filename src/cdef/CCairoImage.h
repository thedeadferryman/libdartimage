//
// Created by kreme on 04.03.2021.
//

#ifndef DARTIMAGE_CCAIROIMAGE_H
#define DARTIMAGE_CCAIROIMAGE_H

#include "../definitions.h"
#include "CSVGImage.h"
#include "CFontUtils.h"
#include "CColorUtils.h"

#define CAIRO_IMAGE(ptr) static_cast<CairoImage *>(ptr)

CDEF_START

typedef void CDEF_EXPORT_CairoImageT;

CDEF_EXPORT_CairoImageT *CDEF_EXPORT_CairoImage_new(int32_t width, int32_t height, CDEF_EXPORT_CairoImage_PixFormat format);

CDEF_EXPORT_CairoImageT *CDEF_EXPORT_CairoImage_newDefault(int32_t width, int32_t height);

CDEF_EXPORT_CairoImageT *CDEF_EXPORT_CairoImage_fromPng(const char *path);

CDEF_EXPORT_DartImage_ErrorStatus CDEF_EXPORT_CairoImage_getStatus(CDEF_EXPORT_CairoImageT *self);

double CDEF_EXPORT_CairoImage_getWidth(CDEF_EXPORT_CairoImageT *self);

double CDEF_EXPORT_CairoImage_getHeight(CDEF_EXPORT_CairoImageT *self);

CDEF_EXPORT_CairoImage_PixFormat CDEF_EXPORT_CairoImage_getPixFmt(CDEF_EXPORT_CairoImageT *self);

void CDEF_EXPORT_CairoImage_drawImage(CDEF_EXPORT_CairoImageT *self, CDEF_EXPORT_CairoImageT *image, double x, double y);

void CDEF_EXPORT_CairoImage_drawImageScaled(CDEF_EXPORT_CairoImageT *self, CDEF_EXPORT_CairoImageT *image, double x, double y,
                                          double scale);

void
CDEF_EXPORT_CairoImage_drawImageScaledAuto(CDEF_EXPORT_CairoImageT *self, CDEF_EXPORT_CairoImageT *image, double x, double y,
                                         double w, double h);

void
CDEF_EXPORT_CairoImage_drawSVG(CDEF_EXPORT_CairoImageT *self, CDEF_EXPORT_SVGImageT *image, double x, double y, double scale);

void CDEF_EXPORT_CairoImage_drawSVGScaled(CDEF_EXPORT_CairoImageT *self, CDEF_EXPORT_SVGImageT *image, double x, double y,
                                        double w, double h);

void CDEF_EXPORT_CairoImage_drawText(CDEF_EXPORT_CairoImageT *self,
                                   const char *text,
                                   CDEF_EXPORT_FontT *font,
                                   CDEF_EXPORT_ColorT *color,
                                   int32_t x, int32_t y,
                                   bool outlined);

CDEF_EXPORT_FontUtils_TextExtents *
CDEF_EXPORT_CairoImage_getFontExtents(CDEF_EXPORT_CairoImageT *self, const char *text, CDEF_EXPORT_FontT *font);

void CDEF_EXPORT_CairoImage_fillRGB(CDEF_EXPORT_CairoImageT *self, CDEF_EXPORT_ColorT *color);

CDEF_EXPORT_CairoImageT *CDEF_EXPORT_CairoImage_resize(CDEF_EXPORT_CairoImageT *self, double scale);

CDEF_EXPORT_CairoImageT *CDEF_EXPORT_CairoImage_resizeAuto(CDEF_EXPORT_CairoImageT *self, double w, double h);

CDEF_EXPORT_CairoImageT *CDEF_EXPORT_CairoImage_clone(CDEF_EXPORT_CairoImageT *self);

CDEF_EXPORT_DartImage_ErrorStatus CDEF_EXPORT_CairoImage_saveAsPNG(CDEF_EXPORT_CairoImageT *self, const char *path);

void CDEF_EXPORT_CairoImage_dispose(CDEF_EXPORT_CairoImageT *self);

CDEF_END

#endif //DARTIMAGE_CCAIROIMAGE_H