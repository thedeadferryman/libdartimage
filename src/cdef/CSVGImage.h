//
// Created by kreme on 04.03.2021.
//

#ifndef DARTIMAGE_CSVGIMAGE_H
#define DARTIMAGE_CSVGIMAGE_H

#include <stdint.h>

#include "../definitions.h"

CDEF_START

typedef void CDEF_EXPORT_SVGImageT;

CDEF_EXPORT_SVGImageT *CDEF_EXPORT_SVGImage_new(const char *path);

int32_t CDEF_EXPORT_SVGImage_getWidth(CDEF_EXPORT_SVGImageT *self);

int32_t CDEF_EXPORT_SVGImage_getHeight(CDEF_EXPORT_SVGImageT *self);

CDEF_EXPORT_DartImage_ErrorStatus CDEF_EXPORT_SVGImage_getStatus(CDEF_EXPORT_SVGImageT *self);

void CDEF_EXPORT_SVGImage_dispose(CDEF_EXPORT_SVGImageT *self);

CDEF_END

#endif //DARTIMAGE_CSVGIMAGE_H
