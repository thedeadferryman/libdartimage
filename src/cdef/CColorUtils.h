//
// Created by Karl Meinkopf on 09.03.2021.
//

#ifndef DARTIMAGE_CCOLORUTILS_H
#define DARTIMAGE_CCOLORUTILS_H

#include "../definitions.h"

#define COLOR(ptr) static_cast<Color *>(ptr)

CDEF_START

typedef void CDEF_EXPORT_ColorT;

CDEF_EXPORT_ColorT *CDEF_EXPORT_ColorUtils_buildRGB(int32_t r, int32_t g, int32_t b);

CDEF_EXPORT_ColorT *CDEF_EXPORT_ColorUtils_buildRGBA(int32_t r, int32_t g, int32_t b, double a);

int32_t CDEF_EXPORT_ColorUtils_getColorComponent(CDEF_EXPORT_ColorT* self, char component);

double CDEF_EXPORT_ColorUtils_getAlpha(CDEF_EXPORT_ColorT* self);

void CDEF_EXPORT_ColorUtils_dispose(CDEF_EXPORT_ColorT *self);

CDEF_END

#endif //DARTIMAGE_CCOLORUTILS_H
