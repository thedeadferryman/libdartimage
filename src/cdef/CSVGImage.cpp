//
// Created by kreme on 04.03.2021.
//

#include "CSVGImage.h"
#include "../dartimage/SVGImage.h"

#define SVG_IMAGE(ptr) static_cast<SVGImage *>(ptr)

// region SVGImage

CDEF_EXPORT_SVGImageT *CDEF_EXPORT_SVGImage_new(const char *path) {
	return new SVGImage(path);
}

int32_t CDEF_EXPORT_SVGImage_getWidth(CDEF_EXPORT_SVGImageT *self) {
	return SVG_IMAGE(self)->getWidth();
}

int32_t CDEF_EXPORT_SVGImage_getHeight(CDEF_EXPORT_SVGImageT *self) {
	return SVG_IMAGE(self)->getHeight();
}

void CDEF_EXPORT_SVGImage_dispose(CDEF_EXPORT_SVGImageT *self) {
	delete SVG_IMAGE(self);
}

CDEF_EXPORT_DartImage_ErrorStatus CDEF_EXPORT_SVGImage_getStatus(CDEF_EXPORT_SVGImageT *self) {
	return SVG_IMAGE(self)->getStatus();
}

// endregion SVGImage
