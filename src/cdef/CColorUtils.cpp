//
// Created by Karl Meinkopf on 09.03.2021.
//

#include "CColorUtils.h"
#include "../dartimage/Color.h"

CDEF_EXPORT_ColorT *CDEF_EXPORT_ColorUtils_buildRGB(int32_t r, int32_t g, int32_t b) {
	return new Color(r, g, b);
}

CDEF_EXPORT_ColorT *CDEF_EXPORT_ColorUtils_buildRGBA(int32_t r, int32_t g, int32_t b, double a) {
	return new Color(r, g, b, a);
}

int32_t CDEF_EXPORT_ColorUtils_getColorComponent(CDEF_EXPORT_ColorT *self, char component) {
	switch (component) {
		case 'r':
		case 'R':
			return COLOR(self)->r;
		case 'g':
		case 'G':
			return COLOR(self)->g;
		case 'b':
		case 'B':
			return COLOR(self)->b;
		default:
			return -1;
	}
}

double CDEF_EXPORT_ColorUtils_getAlpha(CDEF_EXPORT_ColorT *self) {
	return COLOR(self)->a;
}

void CDEF_EXPORT_ColorUtils_dispose(CDEF_EXPORT_ColorT *self) {
	delete COLOR(self);
}
