//
// Created by kreme on 04.03.2021.
//

#include <vector>
#include <cctype>
#include <fontconfig/fontconfig.h>

#include "CFontUtils.h"
#include "../dartimage/FontUtils.h"

#define ToLower(str) std::transform(str.begin(), str.end(), str.begin(), [](unsigned char c) { return std::tolower(c); })


template<typename T>
using Vector = std::vector<T>;

Vector<CDEF_EXPORT_FontUtils_FontDef> getFontList();

bool CDEF_EXPORT_FontUtils_loadFont(const char *path) {
	return FcConfigAppFontAddFile(FcConfigGetCurrent(), reinterpret_cast<const FcChar8 *>(path));
}

bool CDEF_EXPORT_FontUtils_checkFontLoaded(const char *family, CDEF_EXPORT_FontUtils_FontSlant slant,
                                           CDEF_EXPORT_FontUtils_FontWeight weight) {
	auto fonts = getFontList();

	auto sFamily = String(family);

	ToLower(sFamily);

	for (auto font : fonts) {
		auto sFontFamily = String(font.family);

		ToLower(sFontFamily);

		if (sFamily == sFontFamily &&
		    slant == font.slant &&
		    weight == font.weight) {
			return true;
		}
	}

	return false;
}

CDEF_EXPORT_FontUtils_FontList *CDEF_EXPORT_FontUtils_listFontsAvailable() {
	auto fonts = getFontList();

	auto fontsPtr = new CDEF_EXPORT_FontUtils_FontDef[fonts.size()];

	for (auto i = 0; i < fonts.size(); i++) {
		fontsPtr[i] = fonts[i];
	}

	return new CDEF_EXPORT_FontUtils_FontList {
			fontsPtr,
			fonts.size()
	};
}

CDEF_EXPORT_FontT *CDEF_EXPORT_FontUtils_buildFont(const char *fontFace, CDEF_EXPORT_FontUtils_FontSlant slant,
                                                   CDEF_EXPORT_FontUtils_FontWeight weight, double fontSize) {
	auto fDesc = new FontDescription;

	*fDesc = buildFont(
			String(fontFace),
			slant, weight,
			fontSize
	);

	return fDesc;
}

void CDEF_EXPORT_FontUtils_disposeFont(CDEF_EXPORT_FontT *self) {
	delete FONT_DESC(self);
}


Vector<CDEF_EXPORT_FontUtils_FontDef> getFontList() {
	Vector<CDEF_EXPORT_FontUtils_FontDef> fonts;

	auto fConf = FcConfigGetCurrent();
	auto pat = FcPatternCreate();
	auto objSet = FcObjectSetBuild(FC_FAMILY, FC_SLANT, FC_WEIGHT, FC_STYLE, nullptr);

	auto fontList = FcFontList(fConf, pat, objSet);

	for (auto i = 0; i < fontList->nfont; i++) {
		auto font = fontList->fonts[i];

		FcChar8 *fFamily;
		int fSlant, fWeight;

		if (FcPatternGetString(font, FC_FAMILY, 0, &fFamily) == FcResultMatch &&
		    FcPatternGetInteger(font, FC_SLANT, 0, &fSlant) == FcResultMatch &&
		    FcPatternGetInteger(font, FC_WEIGHT, 0, &fWeight) == FcResultMatch) {
			CDEF_EXPORT_FontUtils_FontDef def;

			if ((fSlant != CDEF_EXPORT_FontUtils_FontSlant::SLANT_NORMAL) &&
			    (fSlant != CDEF_EXPORT_FontUtils_FontSlant::SLANT_ITALIC) &&
			    (fSlant != CDEF_EXPORT_FontUtils_FontSlant::SLANT_OBLIQUE)) {
				continue;
			}

			if ((fWeight != CDEF_EXPORT_FontUtils_FontWeight::WEIGHT_NORMAL) &&
			    (fWeight != CDEF_EXPORT_FontUtils_FontWeight::WEIGHT_BOLD)) {
				continue;
			}

			def.slant = CDEF_EXPORT_FontUtils_FontSlant(fSlant);
			def.weight = CDEF_EXPORT_FontUtils_FontWeight(fWeight);

			auto len = strlen(reinterpret_cast<const char *>(fFamily));

			def.family = new char[len + 1];

			strcpy(def.family, reinterpret_cast<const char *>(fFamily));

			fonts.emplace_back(def);
		}
	}

	FcPatternDestroy(pat);
	FcObjectSetDestroy(objSet);

	return fonts;
}
