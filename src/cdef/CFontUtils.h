//
// Created by kreme on 04.03.2021.
//

#ifndef DARTIMAGE_CFONTUTILS_H
#define DARTIMAGE_CFONTUTILS_H

#include "../definitions.h"

#define FONT_DESC(ptr) static_cast<FontDescription *>(ptr)

CDEF_START

typedef struct CDEF_EXPORT_FontUtils_FontDef {
	char *family;
	CDEF_EXPORT_FontUtils_FontSlant slant;
	CDEF_EXPORT_FontUtils_FontWeight weight;
} CDEF_EXPORT_FontUtils_FontDef;

typedef struct CDEF_EXPORT_FontUtils_FontList {
	CDEF_EXPORT_FontUtils_FontDef *fonts;
	size_t size;
} CDEF_EXPORT_FontUtils_FontList;

typedef void CDEF_EXPORT_FontT;

bool CDEF_EXPORT_FontUtils_loadFont(const char *path);

bool CDEF_EXPORT_FontUtils_checkFontLoaded(const char *family, CDEF_EXPORT_FontUtils_FontSlant slant, CDEF_EXPORT_FontUtils_FontWeight weight);

CDEF_EXPORT_FontUtils_FontList *CDEF_EXPORT_FontUtils_listFontsAvailable();

CDEF_EXPORT_FontT *CDEF_EXPORT_FontUtils_buildFont(const char *fontFace,
                                                   CDEF_EXPORT_FontUtils_FontSlant slant, CDEF_EXPORT_FontUtils_FontWeight weight,
                                                   double fontSize);

void CDEF_EXPORT_FontUtils_disposeFont(CDEF_EXPORT_FontT *self);

CDEF_END

#endif //DARTIMAGE_CFONTUTILS_H
