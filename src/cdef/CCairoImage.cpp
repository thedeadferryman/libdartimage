//
// Created by kreme on 02.03.2021.
//

#include <memory>

#include "CCairoImage.h"
#include "../dartimage/CairoImage.h"


// region CairoImage

CDEF_EXPORT_CairoImage_PixFormat formatToExternal(Format format) {
	switch (format) {
	case Format::FORMAT_ARGB32:
		return CDEF_EXPORT_CairoImage_PixFormat::FORMAT_ARGB32;
	case Format::FORMAT_RGB24:
	default:
		return CDEF_EXPORT_CairoImage_PixFormat::FORMAT_RGB24;
	}
}

Format formatToInternal(CDEF_EXPORT_CairoImage_PixFormat format) {
	switch (format) {
	case CDEF_EXPORT_CairoImage_PixFormat::FORMAT_ARGB32:
		return Format::FORMAT_ARGB32;
	case CDEF_EXPORT_CairoImage_PixFormat::FORMAT_RGB24:
	default:
		return Format::FORMAT_RGB24;
	}
}

CDEF_EXPORT_CairoImageT *
CDEF_EXPORT_CairoImage_new(int32_t width, int32_t height, CDEF_EXPORT_CairoImage_PixFormat format) {
	return new CairoImage(width, height, formatToInternal(format));
}

CDEF_EXPORT_CairoImageT *CDEF_EXPORT_CairoImage_newDefault(int32_t width, int32_t height) {
	return CDEF_EXPORT_CairoImage_new(width, height, CDEF_EXPORT_CairoImage_PixFormat::FORMAT_RGB24);
}

CDEF_EXPORT_CairoImageT *CDEF_EXPORT_CairoImage_fromPng(const char *path) {
	return new CairoImage(String(path));
}

double CDEF_EXPORT_CairoImage_getWidth(CDEF_EXPORT_CairoImageT *self) {
	return CAIRO_IMAGE(self)->getWidth();
}

double CDEF_EXPORT_CairoImage_getHeight(CDEF_EXPORT_CairoImageT *self) {
	return CAIRO_IMAGE(self)->getHeight();
}

CDEF_EXPORT_CairoImage_PixFormat CDEF_EXPORT_CairoImage_getPixFmt(CDEF_EXPORT_CairoImageT *self) {
	auto pixfmt = CAIRO_IMAGE(self)->getPixFmt();

	return formatToExternal(pixfmt);
}

void
CDEF_EXPORT_CairoImage_drawImage(CDEF_EXPORT_CairoImageT *self, CDEF_EXPORT_CairoImageT *image, double x, double y) {
	CAIRO_IMAGE(self)->drawImage(*CAIRO_IMAGE(image), x, y);
}

void CDEF_EXPORT_CairoImage_drawImageScaled(CDEF_EXPORT_CairoImageT *self, CDEF_EXPORT_CairoImageT *image, double x,
                                            double y, double scale) {
	CAIRO_IMAGE(self)->drawImage(*CAIRO_IMAGE(image), x, y, scale);
}

void CDEF_EXPORT_CairoImage_drawImageScaledAuto(CDEF_EXPORT_CairoImageT *self, CDEF_EXPORT_CairoImageT *image, double x,
                                                double y, double w, double h) {
	CAIRO_IMAGE(self)->drawImage(*CAIRO_IMAGE(image), x, y, w, h);
}

void CDEF_EXPORT_CairoImage_fillRGB(CDEF_EXPORT_CairoImageT *self, CDEF_EXPORT_ColorT *color) {
	CAIRO_IMAGE(self)->fill(*COLOR(color));
}

CDEF_EXPORT_CairoImageT *CDEF_EXPORT_CairoImage_resize(CDEF_EXPORT_CairoImageT *self, double scale) {
	return new CairoImage(CAIRO_IMAGE(self)->resize(scale));
}

CDEF_EXPORT_CairoImageT *CDEF_EXPORT_CairoImage_resizeAuto(CDEF_EXPORT_CairoImageT *self, double w, double h) {
	return new CairoImage(CAIRO_IMAGE(self)->resize(w, h));
}

CDEF_EXPORT_CairoImageT *CDEF_EXPORT_CairoImage_clone(CDEF_EXPORT_CairoImageT *self) {
	return new CairoImage(CAIRO_IMAGE(self)->clone());
}

CDEF_EXPORT_DartImage_ErrorStatus CDEF_EXPORT_CairoImage_saveAsPNG(CDEF_EXPORT_CairoImageT *self, const char *path) {
	return CAIRO_IMAGE(self)->saveAsPNG(String(path));
}

void CDEF_EXPORT_CairoImage_drawText(CDEF_EXPORT_CairoImageT *self, const char *text, CDEF_EXPORT_FontT *font,
                                     CDEF_EXPORT_ColorT *color, int32_t x, int32_t y,
                                     bool outlined) {
	CAIRO_IMAGE(self)->drawText(
			String(text),
			*FONT_DESC(font),
			*COLOR(color),
			x, y,
			outlined
	);
}

CDEF_EXPORT_FontUtils_TextExtents *
CDEF_EXPORT_CairoImage_getFontExtents(CDEF_EXPORT_CairoImageT *self, const char *text, CDEF_EXPORT_FontT *font) {
	auto te = new CDEF_EXPORT_FontUtils_TextExtents;

	*te = CAIRO_IMAGE(self)->getTextExtents(
			String(text),
			*FONT_DESC(font)
	);

	return te;
}

void CDEF_EXPORT_CairoImage_dispose(CDEF_EXPORT_CairoImageT *self) {
	delete CAIRO_IMAGE(self);
}


void CDEF_EXPORT_CairoImage_drawSVG(CDEF_EXPORT_CairoImageT *self, CDEF_EXPORT_SVGImageT *image, double x, double y,
                                    double scale) {
	CAIRO_IMAGE(self)->drawSVG(*static_cast<SVGImage *>(image), x, y, scale);
}

void
CDEF_EXPORT_CairoImage_drawSVGScaled(CDEF_EXPORT_CairoImageT *self, CDEF_EXPORT_SVGImageT *image, double x, double y,
                                     double w, double h) {
	CAIRO_IMAGE(self)->drawSVG(*static_cast<SVGImage *>(image), x, y, w, h);
}

CDEF_EXPORT_DartImage_ErrorStatus CDEF_EXPORT_CairoImage_getStatus(CDEF_EXPORT_CairoImageT *self) {
	return CAIRO_IMAGE(self)->getStatus();
}

// endregion CairoImage

